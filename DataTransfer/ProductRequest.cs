﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DataTransfer
{
    public class ProductRequest
    {
        public int ProductId { get; set; }
        [Required, StringLength(40)]
        public string ProductName { get; set; }
        [Required, StringLength(40)]
        public string Weight { get; set; }
        [Required]
        public int CategoryId { get; set; }
        [Required]
        [Range(1, Int32.MaxValue)]
        public int UnitsInStock { get; set; }
        [Required]
        [Range(1, Int32.MaxValue)]
        public int UnitPrice { get; set; }
    }
}
