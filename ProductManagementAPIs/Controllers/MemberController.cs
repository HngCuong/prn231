﻿using BusinessObjects;
using DataTransfer;
using Microsoft.AspNetCore.Mvc;
using Repositories;
using Repositories.Interface;

namespace ProductManagementAPIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class MemberController : ControllerBase
    {
        private IMemberRepository repository = new MemberRepository();
        [HttpGet]
        public ActionResult<IEnumerable<Member>> GetMembers() => repository.GetMember();
        [HttpGet("{id}")]
        public ActionResult<Member> GetMemberById(int id) => repository.GetMemberById(id);
        [HttpPost]
        public IActionResult PostMember(MemberRequest memberRequest)
        {
            var member = new Member
            {

                Email = memberRequest.Email,
                CompanyName = memberRequest.CompanyName,
                City = memberRequest.City,
                Country = memberRequest.Country,
                Password = memberRequest.Password,
                Role = "User"
            };
            repository.SaveMember(member);
            return NoContent();
        }
        [HttpDelete("{id}")]
        public IActionResult DeleteMember(int id)
        {
            var c = repository.GetMemberById(id);
            if (c == null)
            {
                return NotFound();
            }
            repository.DeleteMember(c);
            return NoContent();
        }

        [HttpPut("{id}")]
        public IActionResult PutCustomer(int id, MemberRequest memberRequest)
        {
            var cTmp = repository.GetMemberById(id);
            if (cTmp == null)
            {
                return NotFound();
            }

            cTmp.Email = memberRequest.Email;
            cTmp.CompanyName = memberRequest.CompanyName;
            cTmp.City = memberRequest.City;
            cTmp.Country = memberRequest.Country;

            if (memberRequest.Password != null && cTmp.Password != memberRequest.Password)
            {
                cTmp.Password = memberRequest.Password;
            }

            repository.UpdateMember(cTmp);
            return NoContent();
        }
    }
}
