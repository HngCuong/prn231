﻿using BusinessObjects;
using Microsoft.AspNetCore.Mvc;
using Repositories.Interface;
using Repositories;
using DataTransfer;

namespace ProductManagementAPIs.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProductController : ControllerBase
    {
        private IProductRepository repository = new ProductRepository();
        //Get: apu/Products
        [HttpGet]
        public ActionResult<IEnumerable<BusinessObjects.Product>> GetProducts() => repository.GetProducts();
        [HttpGet("{id}")]
        public ActionResult<BusinessObjects.Product> GetProductById(int id) => repository.GetProductById(id);
        //Post : ProductsController/Products
        [HttpPost]
        public IActionResult PostProduct(DataTransfer.ProductRequest productRequest)
        {
            var p = new BusinessObjects.Product
            {
                ProductName = productRequest.ProductName,
                Weight = productRequest.Weight,
                CategoryId = productRequest.CategoryId,
                UnitPrice = productRequest.UnitPrice,
                UnitsInStock = productRequest.UnitsInStock
            };
            repository.SaveProduct(p);
            return NoContent();
        }

        //Get: ProductsController/Delete/5
        [HttpDelete("{id}")]
        public IActionResult DeleteProduct(int id)
        {
            var p = repository.GetProductById(id);
            if (p == null)
                return NotFound();
            repository.DeleteProduct(p);
            return NoContent();
        }
        [HttpPut("{id}")]
        public IActionResult UpdateProduct(int id, DataTransfer.ProductRequest productRequest)
        {
            var pTmp = repository.GetProductById(id);
            if (pTmp == null)
                return NotFound();
            pTmp.ProductName = productRequest.ProductName;
            pTmp.Weight = productRequest.Weight;
            pTmp.CategoryId = productRequest.CategoryId;
            pTmp.UnitPrice = productRequest.UnitPrice;
            pTmp.UnitsInStock = productRequest.UnitsInStock;
            repository.UpdateProduct(pTmp);
            return NoContent();
        }
    }
}
