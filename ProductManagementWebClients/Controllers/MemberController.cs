﻿using BusinessObjects;
using DataTransfer;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;
public class MemberController : Controller
{
    private readonly HttpClient client;
    private string MemberApiUrl = "";

    public MemberController()
    {
        client = new HttpClient();
        var contentType = new MediaTypeWithQualityHeaderValue("application/json");
        client.DefaultRequestHeaders.Accept.Add(contentType);
        MemberApiUrl = "https://localhost:7115/api/Member"; // Điều chỉnh URL API của bạn
    }
    public async Task<IActionResult> Index()
    {
        HttpResponseMessage response = await client.GetAsync(MemberApiUrl);
        string strData = await response.Content.ReadAsStringAsync();
        var options = new JsonSerializerOptions
        {
            PropertyNameCaseInsensitive = true,
        };
        List<MemberRequest> list = new List<MemberRequest>();
        MemberRequest obj = new MemberRequest();
        List<Member> listMebers = JsonSerializer.Deserialize<List<Member>>(strData, options);
        foreach (var item in listMebers)
        {
            obj = new MemberRequest(item.MemberId, item.Email, item.CompanyName, item.City, item.Country, item.Password);
            list.Add(obj);
        }
        return View(list);
    }

    [HttpGet]
    public async Task<IActionResult> Edit(int id)
    {
        try
        {
            // Make a GET request to the API to retrieve the product data
            HttpResponseMessage response = await client.GetAsync($"{MemberApiUrl}/{id}");

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the JSON response to your ProductRequest model
                string strData = await response.Content.ReadAsStringAsync();
                var options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true,
                };
                BusinessObjects.Member memberRequest = JsonSerializer.Deserialize<BusinessObjects.Member>(strData, options);

                if (memberRequest == null)
                {
                    return NotFound();
                }
                List<MemberRequest> list = new List<MemberRequest>();
                MemberRequest obj = new MemberRequest();
                obj = new MemberRequest(memberRequest.MemberId, memberRequest.Email, memberRequest.CompanyName, memberRequest.City, memberRequest.Country, memberRequest.Password);

                // Pass the productRequest to the view for editing
                return View(obj);
            }
            else
            {
                // Handle the case where the API request was not successful
                return NotFound();
            }
        }
        catch (Exception ex)
        {
            // Handle exceptions (e.g., log the error) and return an error view
            ModelState.AddModelError(string.Empty, "Error loading product for editing.");
            return View();
        }
    }

    [HttpPost]
    public async Task<IActionResult> Edit(DataTransfer.MemberRequest memberRequest)
    {

        try
        {
            if (ModelState.IsValid)
            {
                // Serialize the updated productRequest to JSON
                var json = JsonSerializer.Serialize(memberRequest);
                var content = new StringContent(json, Encoding.UTF8, "application/json");
                // Make a PUT request to update the product
                HttpResponseMessage response = await client.PutAsync($"{MemberApiUrl}/{memberRequest.MemberId}", content);
                Debug.WriteLine(response.IsSuccessStatusCode);
                if (response.IsSuccessStatusCode)
                {
                    // Redirect to the Index action upon successful update
                    return RedirectToAction("Index");
                }
                else
                {
                    // Handle the case where the API request to update the product was not successful
                    ModelState.AddModelError(string.Empty, "Error updating product.");
                }
            }

            // If ModelState is not valid or the API request fails, return to the edit view
            return RedirectToAction("Edit");
        }
        catch (Exception ex)
        {
            // Handle exceptions (e.g., log the error) and return to the edit view with an error message
            ModelState.AddModelError(string.Empty, "Error updating product.");
            return View(memberRequest);
        }
    }

    public async Task<IActionResult> Delete(int id)
    {
        HttpResponseMessage response = await client.DeleteAsync($"{MemberApiUrl}/{id}");

        if (response.IsSuccessStatusCode)
        {
            return RedirectToAction("Index");
        }
        else
        {
            return NotFound();
        }
    }

    public async Task<IActionResult> Profile(int myObject)
    {

        try
        {
            // Make a GET request to the API to retrieve the product data
            HttpResponseMessage response = await client.GetAsync($"{MemberApiUrl}/{myObject}");

            if (response.IsSuccessStatusCode)
            {
                // Deserialize the JSON response to your ProductRequest model
                string strData = await response.Content.ReadAsStringAsync();
                var options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true,
                };
                BusinessObjects.Member memberRequest = JsonSerializer.Deserialize<BusinessObjects.Member>(strData, options);

                if (memberRequest == null)
                {
                    return NotFound();
                }
               
                // Pass the productRequest to the view for editing
                return View(memberRequest);
            }
            else
            {
                // Handle the case where the API request was not successful
                return NotFound();
            }
        }
        catch (Exception ex)
        {
            // Handle exceptions (e.g., log the error) and return an error view
            ModelState.AddModelError(string.Empty, "Error loading product for editing.");
            return View();
        }
    }

    public  IActionResult Admin()
    {
      
        return View();
    }
}
