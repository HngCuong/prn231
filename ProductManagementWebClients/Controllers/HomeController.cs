﻿using BusinessObjects;
using DataTransfer;
using Microsoft.AspNetCore.Mvc;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

namespace ProductManagementWebClients.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly HttpClient client;
        private string MemberApiUrl = "";
        public HomeController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            MemberApiUrl = "https://localhost:7115/api/Member"; // Điều chỉnh URL API của bạn
        }
        public async Task<IActionResult> Login(String Email, String Password)
        {
            string email = Email;
            string pass = Password;
           
            HttpResponseMessage response = await client.GetAsync(MemberApiUrl);
            string strData = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };
            List<Member> listMebers = JsonSerializer.Deserialize<List<Member>>(strData, options);
            foreach (var item in listMebers)
            {
                if (item.Email.Equals(email))
                {
                    if (item.Password.Equals(pass))
                    {
                      
                        if (item.Role.Equals("User"))
                        {
                            int myObject = (item.MemberId); // Đối tượng cần truyền
                            var routeValues = new RouteValueDictionary
    {
        { "myObject", myObject }
    };
                            return RedirectToAction("Profile", "Member", routeValues);
                            //return RedirectToAction("Index", "Member");
                        }
                        return RedirectToAction("Admin", "Member");
                    }
                }
            }
            string myString = "Email or Password is not correct";
            TempData["MyString"] = myString;
            return RedirectToAction("Index","Home");
        }


        public IActionResult Index()
        {
            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }
     
     

    }
}