﻿using BusinessObjects;
using DataTransfer;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.Net.Http.Headers;
using System.Text;
using System.Text.Json;

namespace ProductManagementWebClients.Controllers
{
    public class ProductController : Controller
    {
        private readonly HttpClient client = null;
        private string ProductApiUrl = "";
        public ProductController()
        {
            client = new HttpClient();
            var contentType = new MediaTypeWithQualityHeaderValue("application/json");
            client.DefaultRequestHeaders.Accept.Add(contentType);
            ProductApiUrl = "https://localhost:7115/api/Product";

        }
        public async Task<IActionResult> Index()
        {
            HttpResponseMessage response = await client.GetAsync(ProductApiUrl);
            string strData = await response.Content.ReadAsStringAsync();
            var options = new JsonSerializerOptions
            {
                PropertyNameCaseInsensitive = true,
            };
            List<BusinessObjects.Product> listProducts = JsonSerializer.Deserialize<List<BusinessObjects.Product>>(strData, options);
;            return View(listProducts);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(DataTransfer.ProductRequest product)
        {
            if (ModelState.IsValid)
            {
                var json = JsonSerializer.Serialize(product);
                var content = new StringContent(json, Encoding.UTF8, "application/json");

                HttpResponseMessage response = await client.PostAsync(ProductApiUrl, content);

                if (response.IsSuccessStatusCode)
                {
                    return RedirectToAction("Index");
                }
                else
                {
                    ModelState.AddModelError(string.Empty, "Error creating product.");
                }
            }

            return View(product);
        }

        [HttpGet]
        public async Task<IActionResult> Edit(int id)
        {
            try
            {
                // Make a GET request to the API to retrieve the product data
                HttpResponseMessage response = await client.GetAsync($"{ProductApiUrl}/{id}");

                if (response.IsSuccessStatusCode)
                {
                    // Deserialize the JSON response to your ProductRequest model
                    string strData = await response.Content.ReadAsStringAsync();
                    var options = new JsonSerializerOptions
                    {
                        PropertyNameCaseInsensitive = true,
                    };
                    BusinessObjects.Product productRequest = JsonSerializer.Deserialize<BusinessObjects.Product>(strData, options);

                    if (productRequest == null)
                    {
                        return NotFound();
                    }

                    // Pass the productRequest to the view for editing
                    return View(productRequest);
                }
                else
                {
                    // Handle the case where the API request was not successful
                    return NotFound();
                }
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., log the error) and return an error view
                ModelState.AddModelError(string.Empty, "Error loading product for editing.");
                return View();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(DataTransfer.ProductRequest productRequest)
        {
      
            try
            {
                if (ModelState.IsValid)
                {
                    // Serialize the updated productRequest to JSON
                    var json = JsonSerializer.Serialize(productRequest);
                    var content = new StringContent(json, Encoding.UTF8, "application/json");
                    // Make a PUT request to update the product
                    HttpResponseMessage response = await client.PutAsync($"{ProductApiUrl}/{productRequest.ProductId}", content);
                    Debug.WriteLine(response.IsSuccessStatusCode);
                    if (response.IsSuccessStatusCode)
                    {
                        // Redirect to the Index action upon successful update
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        // Handle the case where the API request to update the product was not successful
                        ModelState.AddModelError(string.Empty, "Error updating product.");
                    }
                }

                // If ModelState is not valid or the API request fails, return to the edit view
                return RedirectToAction("Edit");
            }
            catch (Exception ex)
            {
                // Handle exceptions (e.g., log the error) and return to the edit view with an error message
                ModelState.AddModelError(string.Empty, "Error updating product.");
                return View(productRequest);
            }
        }


        public async Task<IActionResult> Details(int id)
        {
            HttpResponseMessage response = await client.GetAsync($"{ProductApiUrl}/{id}");
            if (response.IsSuccessStatusCode)
            {
                string strData = await response.Content.ReadAsStringAsync();
                var options = new JsonSerializerOptions
                {
                    PropertyNameCaseInsensitive = true,
                };
                BusinessObjects.Product product = JsonSerializer.Deserialize<BusinessObjects.Product>(strData, options);

                if (product == null)
                {
                    return NotFound();
                }

                return View(product);
            }
            else
            {
                return NotFound();
            }
        }

        public async Task<IActionResult> Delete(int id)
        {
            HttpResponseMessage response = await client.DeleteAsync($"{ProductApiUrl}/{id}");

            if (response.IsSuccessStatusCode)
            {
                return RedirectToAction("Index");
            }
            else
            {
                return NotFound();
            }
        }
    }
}

