﻿using BusinessObjects;
using DataAccess;
using Repositories.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories
{
    public class MemberRepository : IMemberRepository
    {
        public void DeleteMember(Member member) => MemberDAO.DeleteMember(member);

        public List<Member> GetMember() => MemberDAO.GetMembers();

        public Member GetMemberById(int id) => MemberDAO.FindMemberById(id);
        public void SaveMember(Member member) => MemberDAO.SaveMember(member);

        public void UpdateMember(Member member) => MemberDAO.UpdateMember(member);
    }
}
