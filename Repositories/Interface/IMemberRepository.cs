﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Interface
{
    public interface IMemberRepository
    {
        void SaveMember(Member member);
        Member GetMemberById(int id);
        List<Member> GetMember();
        void UpdateMember(Member member);
        void DeleteMember(Member member);
    }
}
