﻿using BusinessObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Repositories.Interface
{
    public interface IProductRepository
    {
        void SaveProduct(Product p);
        Product GetProductById(int id);
        List<Product> Search(string keyword);
        void UpdateProduct(Product p);
        void DeleteProduct(Product p);
        List<Product> GetProducts();
        List<Category> GetCategories();
    }
}
